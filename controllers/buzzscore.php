<?php

  // require_once BUZZTRACE_API_PATH . "models/Buzzscore.php";

   global $db;

  if (isset($params->user_id)){

      $sql = "
        SELECT
          tb.twitter_total_followers AS twitter,
          tb.facebook_followers AS facebook,
          tb.goodreads_followers AS goodreads,
          tb.score_date AS date
        FROM tracker_buzzscore tb
        WHERE tb.user_id = '". $params->user_id ."'
        AND tb.score_date IN (
          SELECT MAX(score_date)
          FROM tracker_buzzscore
            WHERE user_id = '". $params->user_id ."'
        )
      ";

      $followers = $db->get_results($sql)[0];

      $data->followers = new stdClass();
      $data->followers->twitter = 0;
      $data->followers->facebook = 0;
      $data->followers->goodreads = 0;

      if(isset($followers->twitter)){
        $data->followers->twitter = $followers->twitter;
      }
      if(isset($followers->facebook)){
        $data->followers->facebook = $followers->facebook;
      }
      if(isset($followers->goodreads)){
        $data->followers->goodreads = $followers->goodreads;
      }

    // $sql = "
    //   SELECT
    //     user_id,
    //     MAX(score_date),
    //     best_amazon_rank,
    //     facebook_daily_engagements,
    //     twitter_total_followers,
    //     twitter_retweet_count,
    //     task_count_average,
    //     facebook_followers,
    //     goodreads_followers
    //   FROM tracker_buzzscore
    //   GROUP BY user_id
    //   ";

      $sql = "
        SELECT
          user_id,
          score_date,
          best_amazon_rank,
          facebook_daily_engagements,
          twitter_total_followers,
          twitter_retweet_count,
          task_count_average,
          facebook_followers,
          goodreads_followers
        FROM tracker_buzzscore
        WHERE score_date IN (SELECT MAX(score_date) FROM tracker_buzzscore GROUP BY user_id)
      ";

    $res = $db->get_results($sql);

    $sales_nonzero = array();
    $sales_zero = array();
    $fb = array();
    $twitter = array();
    $task = array();
    $buzzscore = array();

    // 100 / count * order

    $placevalue = 100 / count($res);

    foreach($res as $user){
      if($user->best_amazon_rank > 0){
        $sales_nonzero[$user->user_id] = $user->best_amazon_rank;
      } else {
        $sales_zero[$user->user_id] = $user->best_amazon_rank;
      }

      $fb[$user->user_id] = $user->facebook_daily_engagements;
      $twitter[$user->user_id] = $user->twitter_retweet_count;
      $task[$user->user_id] = $user->task_count_average;
    }

    arsort($sales_nonzero);
    asort($fb);
    asort($twitter);
    asort($task);

    $i = 1;
    foreach ($sales_zero as $user_id => $value){
      $buzzscore[$user_id]['sales'] = floor($i * $placevalue);
      $i++;
    }
    foreach ($sales_nonzero as $user_id => $value){
      $buzzscore[$user_id]['sales'] = floor($i * $placevalue);
      $i++;
    }


    $i = 1;
    foreach ($fb as $user_id => $value){
      $buzzscore[$user_id]['fb'] = floor($i * $placevalue);
      $i++;
    }

    $i = 1;
    foreach ($twitter as $user_id => $value){
      $buzzscore[$user_id]['twitter'] = floor($i * $placevalue);
      $i++;
    }

    $i = 1;
    foreach ($task as $user_id => $value){
      $buzzscore[$user_id]['task'] = floor($i * $placevalue);
      $i++;
    }

    foreach ($buzzscore as $user_id => $percentiles){
      $sum = 0;
      $sum += $percentiles['sales'];
      $buzzscore[$user_id]['sales'] = floor($percentiles['sales']);
      if ($percentiles['fb'] > $percentiles['twitter']){
        $sum += $percentiles['fb'];
        $buzzscore[$user_id]['social'] = floor($percentiles['fb']);
      } else {
        $sum += $percentiles['twitter'];
        $buzzscore[$user_id]['social'] = floor($percentiles['twitter']);
      }
      $sum += $percentiles['task'];
      $buzzscore[$user_id]['task'] = floor($percentiles['task']);

      $buzzscore[$user_id]['score'] = floor($sum / 3);

    }

    $data->buzzscore = $buzzscore[$params->user_id];

  //  $data->res = $res;



  //  $data->test = "nooch";


  } else {
    $data->error = "This request doesn't even have an user_id parameter set. There's literally nothing I can do here. ";
  }




?>
