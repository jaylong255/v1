<?php



   require_once BUZZTRACE_API_PATH . "models/Sales.php";




  if (isset($params->isbn)){
    $isbn = $params->isbn;
    if (isset($params->record_type)){
      if ($params->record_type == "ranks"){
        $data->error = "so far so good. ";
        $args = new stdClass();
        if(isset($params->market)){
          $args->market = $params->market;
        }
        if(isset($params->start)){
          $args->start = $params->start;
        }
        if(isset($params->frequency)){
          $args->frequency = $params->frequency;
        }
        if(isset($params->duration)){
          $args->duration = $params->duration;
        }

        $data->ranks = array();


        if(isset($params->markets)){
          foreach($params->markets as $market){
            $args->market = $market;
            $sales = new Amazon_Sales($isbn,$args);
            $data->ranks[str_replace('.','',$market)] = $sales->get_rank_set($params) or $data->error = "Attempt to query sales ranks failed. ";
          }
        } else {
          $sales = new Amazon_Sales($isbn,$args);
          $data->ranks = $sales->get_rank_set($params) or $data->error = "Attempt to query sales ranks failed. ";
        }




    //  $data->ranks = "so far so good. ";

      } elseif ($params->record_type == "current"){

        // $sales = new Amazon_Sales($isbn)
        $sql = "
          SELECT
            ta.rank_time AS time,
            ta.salesrank AS rank,
            ta.market AS market
          FROM tracker_amazon_salesrank ta
          WHERE isbn = '". $isbn ."'

        ";
        global $db;
        $res = $db->get_results($sql);
        $sales_ranks = array();
        foreach($res as $row){
          $sales_ranks[$row->market] = $row->rank;
        }

        $data->sales_ranks = array(
          'us'  =>  $sales_ranks['.com'],
          'ca'  =>  $sales_ranks['.ca'],
          'uk'  =>  $sales_ranks['.co.uk']
        );

      } else {
        $data->error = "I don't know what record type this is. Try 'ranks'. ";
      }
    } else {
      $data->error = "I don't know what type of record you're trying to access. Please send a record_type parameter. (try 'ranks').";
    }
  } else {
    $data->error = "This request doesn't even have an isbn parameter set. There's literally nothing I can do here. ";
  }

?>
