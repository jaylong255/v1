<?php

  // die('nooch');

  $params->user_id = 57;


  $duration = 60 * 60 * 24 * 30;
  $params->duration = $duration;

  // die($params->duration);

  // require_once BUZZTRACE_API_PATH . "models/Buzzscore.php";

   global $db;

   //check for user id or throw error
  if (isset($params->user_id)){

    //for average buzzscore, we need to loop through a date range
    if(isset($params->duration)){

      if(isset($params->start)){
        $start = $params->start;
      } else {
        $start = time() - $params->duration;
      }
      $end = $start + $params->duration;
      // $sql = "
      //   SELECT
      //     tb.twitter_total_followers AS twitter,
      //     tb.facebook_followers AS facebook,
      //     tb.goodreads_followers AS goodreads,
      //     date(score_date) AS date,
      //     tb.daily_buzzscore AS buzzscore,
      //     tb.sales_percentile AS sales,
      //     tb.social_percentile AS social,
      //     tb.task_percentile AS task
      //   FROM tracker_buzzscore tb
      //   WHERE tb.user_id = '". $params->user_id ."'
      //   AND tb.score_date >= '". date('Y-m-d',$start) ." 00:00:00'
      //   AND tb.score_date <= '". date('Y-m-d',$end) ." 23:59:59'
      //
      // ";

      $dates = array();
      for( $i = $start; $i <= $end; $i += (60*60*24) ){
        $dates[] = " date(score_date) = '". date('Y-m-d',$i) ."' ";
      }

      $sql = "
        SELECT DISTINCT
          date(score_date) AS date,
          tb.user_id,
          tb.daily_buzzscore AS buzzscore,
          tb.sales_percentile AS sales,
          tb.social_percentile AS social,
          tb.task_percentile AS task
        FROM tracker_buzzscore tb
        WHERE tb.user_id = '". $params->user_id ."' AND
        (". implode(' OR ', $dates) .")
      ";

        // die($sql);

      // $sql = "
      //   SELECT
      //     tb.twitter_total_followers AS twitter,
      //     tb.facebook_followers AS facebook,
      //     tb.goodreads_followers AS goodreads,
      //     tb.score_date AS date
      //   FROM tracker_buzzscore tb
      //   WHERE tb.user_id = '". $params->user_id ."'
      //   AND tb.score_date IN (
      //     SELECT distinct date(score_date)
      //     FROM tracker_buzzscore
      //       WHERE user_id = '". $params->user_id ."'
      //       AND score_date >= '". date('Y-m-d',$start) ." 00:00:00'
      //       AND score_date <= '". date('Y-m-d',$end) ." 23:59:59'
      //   )
      // ";



      $results = $db->get_results($sql);

      // $records = array();
      // foreach($results as $record){
      //   $records[$record->date]
      // }

      // var_dump($results);
      // die($sql);

      $buzzscores = array();

      foreach ($results as $record){

        if(isset($record->buzzscore)){
          $buzzscores[] = $record->buzzscore;
        } else {
          $buzzscores[] = calculate_and_store_buzzscore(date('Y-m-d',strtotime($record->date)))[$params->user_id]['score'];
           $record->buzzscore;
        }

      }

      // var_dump($buzzscores);
      // die();


      if(count($buzzscores > 0)){
        $data->buzzscore->score = array_sum($buzzscores) / count($buzzscores);
      }


    } else {

      //for most recent buzzscore we can assume max date, we don't need a loop
      $sql = "
        SELECT
          tb.twitter_total_followers AS twitter,
          tb.facebook_followers AS facebook,
          tb.goodreads_followers AS goodreads,
          tb.score_date AS date,
          tb.daily_buzzscore AS buzzscore,
          tb.sales_percentile AS sales,
          tb.social_percentile AS social,
          tb.task_percentile AS task
        FROM tracker_buzzscore tb
        WHERE tb.user_id = '". $params->user_id ."'
        AND tb.score_date IN (
          SELECT MAX(score_date)
          FROM tracker_buzzscore
            WHERE user_id = '". $params->user_id ."'
        )
      ";

      $followers = $db->get_results($sql)[0];

      //also, we only care about followers on the max date query.
      //custom ranges get buzzscore data only
      $data->followers = new stdClass();
      $data->followers->twitter = 0;
      $data->followers->facebook = 0;
      $data->followers->goodreads = 0;

      if(isset($followers->twitter)){
        $data->followers->twitter = $followers->twitter;
      }
      if(isset($followers->facebook)){
        $data->followers->facebook = $followers->facebook;
      }
      if(isset($followers->goodreads)){
        $data->followers->goodreads = $followers->goodreads;
      }

      if(isset($followers->buzzscore)){
        $data->buzzscore->score = $followers->buzzscore;
        $data->buzzscore->sales = $followers->sales;
        $data->buzzscore->social = $followers->social;
        $data->buzzscore->task = $followers->task;

      } else {

          //
          //
          // ////////////////////////////////////////
          // //here is where we start the loop... i think
          // /////////////////////////
          //
          //
          //
          //     //this query should be very similar to each query in the loop.
          //     //we will check for the buzzscore field. if it's null we will run
          //     //pretty much this same query only the array will have a date that
          //     //we pass in instead of doing max date. then we will do almost
          //     //everything in this else statement passing a different date ranges
          //     //each time. all this code should be put in a function and reused
          //     $sql = "
          //       SELECT
          //         user_id,
          //         score_date,
          //         best_amazon_rank,
          //         facebook_daily_engagements,
          //         twitter_total_followers,
          //         twitter_retweet_count,
          //         task_count_average,
          //         facebook_followers,
          //         goodreads_followers,
          //         daily_buzzscore,
          //         sales_percentile,
          //         social_percentile,
          //         task_percentile
          //       FROM tracker_buzzscore
          //       WHERE score_date IN (SELECT MAX(score_date) FROM tracker_buzzscore GROUP BY user_id)
          //     ";
          //
          //   $res = $db->get_results($sql);
          //
          //
          //
          //
          //
          //
          //   //this score_date variable will be defined in the foreach loop
          //   // so the function will accept score_date as an arguement
          //   // a simple SELECT MAX(score_date) query will provide this for the
          //   //original request of max date
          //   $score_date = strtotime(date('Y-m-d',strtotime($res[0]->score_date)));
          //
          //   $sales_nonzero = array();
          //   $sales_zero = array();
          //   $fb = array();
          //   $twitter = array();
          //   $task = array();
          //   $buzzscore = array();
          //
          //   // 100 / count * order
          //
          //   $placevalue = 100 / count($res);
          //
          //   foreach($res as $user){
          //     if($user->best_amazon_rank > 0){
          //       $sales_nonzero[$score_date][$user->user_id] = $user->best_amazon_rank;
          //     } else {
          //       $sales_zero[$score_date][$user->user_id] = $user->best_amazon_rank;
          //     }
          //
          //     $fb[$score_date][$user->user_id] = $user->facebook_daily_engagements;
          //     $twitter[$score_date][$user->user_id] = $user->twitter_retweet_count;
          //     $task[$score_date][$user->user_id] = $user->task_count_average;
          //   }
          //
          //   arsort($sales_nonzero[$score_date]);
          //   asort($fb[$score_date]);
          //   asort($twitter[$score_date]);
          //   asort($task[$score_date]);
          //
          //   $i = 1;
          //   foreach ($sales_zero[$score_date] as $user_id => $value){
          //     $buzzscore[$score_date][$user_id]['sales'] = floor($i * $placevalue);
          //     $i++;
          //   }
          //   foreach ($sales_nonzero[$score_date] as $user_id => $value){
          //     $buzzscore[$score_date][$user_id]['sales'] = floor($i * $placevalue);
          //     $i++;
          //   }
          //
          //
          //   $i = 1;
          //   foreach ($fb[$score_date] as $user_id => $value){
          //     $buzzscore[$score_date][$user_id]['fb'] = floor($i * $placevalue);
          //     $i++;
          //   }
          //
          //   $i = 1;
          //   foreach ($twitter[$score_date] as $user_id => $value){
          //     $buzzscore[$score_date][$user_id]['twitter'] = floor($i * $placevalue);
          //     $i++;
          //   }
          //
          //   $i = 1;
          //   foreach ($task[$score_date] as $user_id => $value){
          //     $buzzscore[$score_date][$user_id]['task'] = floor($i * $placevalue);
          //     $i++;
          //   }
          //
          //   foreach ($buzzscore[$score_date] as $user_id => $percentiles){
          //     $sum = 0;
          //     $sum += $percentiles['sales'];
          //     $buzzscore[$score_date][$user_id]['sales'] = floor($percentiles['sales']);
          //     if ($percentiles['fb'] > $percentiles['twitter']){
          //       $sum += $percentiles['fb'];
          //       $buzzscore[$score_date][$user_id]['social'] = floor($percentiles['fb']);
          //     } else {
          //       $sum += $percentiles['twitter'];
          //       $buzzscore[$score_date][$user_id]['social'] = floor($percentiles['twitter']);
          //     }
          //     $sum += $percentiles['task'];
          //     $buzzscore[$score_date][$user_id]['task'] = floor($percentiles['task']);
          //
          //     $buzzscore[$score_date][$user_id]['score'] = floor($sum / 3);
          //
          //     $update = "
          //       UPDATE tracker_buzzscore
          //       SET daily_buzzscore = '". $buzzscore[$score_date][$user_id]['score'] ."',
          //       SET sales_percentile = '". $buzzscore[$score_date][$user_id]['sales'] ."',
          //       SET task_percentile = '". $buzzscore[$score_date][$user_id]['task'] ."',
          //       SET social_percentile = '". $buzzscore[$score_date][$user_id]['social'] ."'
          //       WHERE user_id = '". $user_id ."'
          //       AND score_date >= '". $score_date ." 00:00:00'
          //       AND score_date <= '". $score_date ." 23:59:59'
          //     ";
          //
          //     $db->query($update);
          //
          //
          //
          //   }
          //
          //   $data->buzzscore = $buzzscore[$score_date][$params->user_id];


          $sql = "SELECT MAX(score_date) FROM tracker_buzzscore";
          $score_date = $db->query($sql)[0]->score_date;
          $data->buzzscore = calculate_and_store_buzzscore($score_date)[$params->user_id];


      }


    }




  } else {
    $data->error = "This request doesn't even have an user_id parameter set. There's literally nothing I can do here. ";
  }



  function calculate_and_store_buzzscore($score_date){




              ////////////////////////////////////////
              //here is where we start the loop... i think
              /////////////////////////



                  //this query should be very similar to each query in the loop.
                  //we will check for the buzzscore field. if it's null we will run
                  //pretty much this same query only the array will have a date that
                  //we pass in instead of doing max date. then we will do almost
                  //everything in this else statement passing a different date ranges
                  //each time. all this code should be put in a function and reused
                  $sql = "
                    SELECT
                      user_id,
                      score_date,
                      best_amazon_rank,
                      facebook_daily_engagements,
                      twitter_total_followers,
                      twitter_retweet_count,
                      task_count_average,
                      facebook_followers,
                      goodreads_followers,
                      daily_buzzscore,
                      sales_percentile,
                      social_percentile,
                      task_percentile
                    FROM tracker_buzzscore
                    WHERE score_date >= '". $score_date ." 00:00:00'
                    AND score_date <= '". $score_date ." 23:59:59'

                  ";
                  global $db;
                  // die($sql);
                $res = $db->get_results($sql);

                  // var_dump($res);

                //this score_date variable will be defined in the foreach loop
                // so the function will accept score_date as an arguement
                // a simple SELECT MAX(score_date) query will provide this for the
                //original request of max date
                // $score_date = strtotime(date('Y-m-d',strtotime($res[0]->score_date)));

                $sales_nonzero = array();
                $sales_zero = array();
                $fb = array();
                $twitter = array();
                $task = array();
                $buzzscore = array();

                // 100 / count * order

                $placevalue = 100 / count($res);

                foreach($res as $user){
                  if($user->best_amazon_rank > 0){
                    $sales_nonzero[$score_date][$user->user_id] = $user->best_amazon_rank;
                  } else {
                    $sales_zero[$score_date][$user->user_id] = $user->best_amazon_rank;
                  }

                  $fb[$score_date][$user->user_id] = $user->facebook_daily_engagements;
                  $twitter[$score_date][$user->user_id] = $user->twitter_retweet_count;
                  $task[$score_date][$user->user_id] = $user->task_count_average;
                }

                arsort($sales_nonzero[$score_date]);
                asort($fb[$score_date]);
                asort($twitter[$score_date]);
                asort($task[$score_date]);

                $i = 1;
                foreach ($sales_zero[$score_date] as $user_id => $value){
                  $buzzscore[$score_date][$user_id]['sales'] = floor($i * $placevalue);
                  $i++;
                }
                foreach ($sales_nonzero[$score_date] as $user_id => $value){
                  $buzzscore[$score_date][$user_id]['sales'] = floor($i * $placevalue);
                  $i++;
                }


                $i = 1;
                foreach ($fb[$score_date] as $user_id => $value){
                  $buzzscore[$score_date][$user_id]['fb'] = floor($i * $placevalue);
                  $i++;
                }

                $i = 1;
                foreach ($twitter[$score_date] as $user_id => $value){
                  $buzzscore[$score_date][$user_id]['twitter'] = floor($i * $placevalue);
                  $i++;
                }

                $i = 1;
                foreach ($task[$score_date] as $user_id => $value){
                  $buzzscore[$score_date][$user_id]['task'] = floor($i * $placevalue);
                  $i++;
                }

                foreach ($buzzscore[$score_date] as $user_id => $percentiles){
                  $sum = 0;
                  $sum += $percentiles['sales'];
                  $buzzscore[$score_date][$user_id]['sales'] = floor($percentiles['sales']);
                  if ($percentiles['fb'] > $percentiles['twitter']){
                    $sum += $percentiles['fb'];
                    $buzzscore[$score_date][$user_id]['social'] = floor($percentiles['fb']);
                  } else {
                    $sum += $percentiles['twitter'];
                    $buzzscore[$score_date][$user_id]['social'] = floor($percentiles['twitter']);
                  }
                  $sum += $percentiles['task'];
                  $buzzscore[$score_date][$user_id]['task'] = floor($percentiles['task']);

                  $buzzscore[$score_date][$user_id]['score'] = floor($sum / 3);

                  $update = "
                    UPDATE tracker_buzzscore
                    SET daily_buzzscore = '". $buzzscore[$score_date][$user_id]['score'] ."',
                    sales_percentile = '". $buzzscore[$score_date][$user_id]['sales'] ."',
                    task_percentile = '". $buzzscore[$score_date][$user_id]['task'] ."',
                    social_percentile = '". $buzzscore[$score_date][$user_id]['social'] ."'
                    WHERE user_id = '". $user_id ."'
                    AND date(score_date) = '". $score_date ."'
                  ";

                   $db->query($update);

                  // echo $update . "<br />";
                  //
                  // die();


                }

                return $buzzscore[$score_date];


  }



?>
