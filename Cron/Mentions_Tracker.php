<?php

  $bing_logfile = "bing_logfile.txt";
  $fh = fopen($bing_logfile,'a');
  fwrite($fh, date('Y-m-d h:i:s') . " Starting up the bing script.  \n. ");
  fclose($fh);


  $lock_file = "bing_lockfile.txt";

  if(file_exists($lock_file)){
    $bing_logfile = "bing_logfile.txt";
    $fh = fopen($bing_logfile,'a');
    fwrite($fh, date('Y-m-d h:i:s') . " Lock file is present. Exiting script.  \n. ");
    fclose($fh);
    exit;
  } else {
    $fh = fopen($lock_file,"w");
    if(!$fh){
      $bing_logfile = "bing_logfile.txt";
      $fh = fopen($bing_logfile,'a');
      fwrite($fh, date('Y-m-d h:i:s') . " Failed to create lock file. .  \n. ");
      fclose($fh);
      exit;
    }
  }

  $bing_logfile = "bing_logfile.txt";
  $fh = fopen($bing_logfile,'a');
  fwrite($fh, date('Y-m-d h:i:s') . " Made it past lock file check. Continuing script.  \n. ");
  fclose($fh);

  $bing_callcounter = 0;

  if(!isset($api_response)){
    $api_response = new stdClass();
  }

//require_once BUZZTRACE_API_PATH . "interfaces/Bing.php";
require_once BUZZTRACE_API_PATH . "models/Book.php";
require_once BUZZTRACE_API_PATH . "models/Error.php";
require_once BUZZTRACE_API_PATH .'third_party/twitter/vendor/autoload.php';

$error = new Error_log(array(
  'script' => "Mentions_Tracker.php",
  'subject_name' => "Bing",
  'subject_type' => "tracker_script"
));

if(isset($params->isbn)){
  $isbn_query = "isbn=" . $params->isbn;
} else {
  $isbn_query = "";
}


  //this link gets the buzztrace site to spit out a json with all of the isbns that the users are tracking
  $json = file_get_contents(BUZZTRACE_LIVE_URL . "wp-content/plugins/buzztrace/src/Admin/api_share_booklist.php?" . $isbn_query);
  $data = json_decode($json);

  $api_response->snoogens = $data;
  var_dump($api_response);

  global $db;

  foreach($data->isbns as $isbn){
      if ($isbn_id = track_isbn($isbn)){
        $record = get_isbn_record($isbn_id);
        //echo $isbn . " inserted successfully - id# " . $isbn_id . " - tracking: ". $record->tracking ."\n";
      } else {
        //echo $isbn . " - failed to insert! \n";
      }
  }

  foreach($data->books as $datum){
    $isbn = $datum->isbn;
    if(strlen($isbn) != 10){
      echo "Invalid isbn: Not a 10 character string. \n";
      $error->log("Invalid isbn. Not a 10 character string. ",array(
        'subject_type'=> "isbn",
        'subject_id'=> $isbn,
        'subject_name'=>"item lookup"
      ));
    } else {

      // $isbn_id = track_isbn($isbn);
      // $record = get_isbn_record($isbn_id);
      // echo "Getting twitter token for isbn# " . $isbn . " - ". $datum->title ."\n";

      $args = array(
        'isbns' => array($isbn)
      );

      $title = $datum->title;
      if(isset($title) && $title && $title != ""){
        $args['title'] = $title;
      }

      $author = $datum->author;
      if(isset($author) && $author && $author != ""){
        $args['authors'] = array($author);
      }

      if($book = new Book($args)){
//         if($book->update($args)){
//
//         echo "BOOK OBJECT FOUND/ADDED FOR '". $book->title ."' by '". $author ."' ISBN: '". $isbn ."'
// ";
//                   // var_dump($book);
//
//         } else {
//             var_dump($args);
//             echo $book->title . " FAILED TO UPDATE. " . $isbn . " book_id: " . $book->id . "\n";
//         }

        $book->update($args);

        echo "BOOK OBJECT FOUND/ADDED FOR '". $book->title ."' by '". $author ."' ISBN: '". $isbn ."'
";

      } else {
        var_dump($args);
        echo $book->title . " FAILED HORRIBLY. FIGURE IT OUT. " . $isbn . " book_id: " . $book->id . "\n";
      }

    }
  }

  // $json = file_get_contents(BUZZTRACE_BETA_URL . "wp-content/plugins/buzztrace/src/Admin/api_share_booklist.php");
  // $data = json_decode($json);
  //
  // foreach($data->isbns as $isbn){
  //     if ($isbn_id = track_isbn($isbn)){
  //       $record = get_isbn_record($isbn_id);
  //       echo $isbn . " inserted successfully - id# " . $isbn_id . " - tracking: ". $record->tracking ."\n";
  //     } else {
  //       echo $isbn . " - failed to insert! \n";
  //     }
  // }

  $isbns = get_isbn_list(1);

  if(isset($params->isbn)){
    $isbns = array($params->isbn);
  }

//  $isbns = array("0486411095"); //dracula



// $nooch = $data->books;
// $data->books = array();
//
// // $i = 20;
//
// for($i=0;$i<10;$i++){
//   $data->books[] = $nooch[$i];
// }

// die(":::" . $_REQUEST['isbn'] . ":::");

foreach($data->books as $datum){

  //skip twitter
  //continue;

  if(isset($_REQUEST['isbn'])){
    if($datum->isbn == $_REQUEST['isbn']){
      // continue;
      // var_dump($datum);
      // echo $_REQUEST['isbn'] . ':::' . $datum->isbn . '\n';
      // die();
    } else {
      // echo $_REQUEST['isbn'] . ':::' . $datum->isbn . '\n';

    }
    // var_dump($_REQUEST['isbn'] . ':::' . $datum->isbn);
  }

  // continue;




  $isbn = $datum->isbn;


  if(strlen($isbn) != 10){
    echo "Invalid isbn: Not a 10 character string. \n";
    $error->log("Invalid isbn. Not a 10 character string. ",array(
      'subject_type'=> "isbn",
      'subject_id'=> $isbn,
      'subject_name'=>"item lookup"
    ));
  } else {

    $isbn_id = track_isbn($isbn);
    $record = get_isbn_record($isbn_id);
    echo "Getting twitter token for isbn# " . $isbn . " - ". $datum->title ."\n";

    $args = array(
      'title'=>$datum->title,
      'isbns'=>array($isbn)
    );



    $book = new Book($args);

    // var_dump($book);

    $access_token = unserialize($datum->twitter_token);
    // $token =
    // $access_token = unserialize($access_token);
    $twitter_connection = new \Abraham\TwitterOAuth\TwitterOAuth(
      '5y0ITmaFvUSiNrcPin0oKysEF',
      'tS6VrThOBDVDQmOAtLEksw5OPOCSio69Yf3vsU5ZLyFphzc2qn',
      $access_token['oauth_token'], $access_token['oauth_token_secret']
    );

    if(isset($book->authors[0])){
      $author = $book->authors[0];
    } else {
      $author = "";
    }

    $title = $book->title;


    if(strpos($title,':')){
      $parts = explode(":",$title);
      $title = $parts[0];
    }

    if(strpos($title,'(')){
      $parts = explode("(",$title);
      $title = $parts[0];
    }

    // $title = str_replace(' ','+',$title);
    // $author = str_replace(' ','+',$author);

    // $author = "tim shaw";
      // $query = $title;
      // $query = '"'. $title .'"' . "+" . '"'. $author .'"';

      $query = rawurlencode($title . " " . $author);
      // die($query);
      // $query = '"'. $title .'"';
      // $query = $title . " " . $author;

      var_dump($query);

    $search = $twitter_connection->get("search/tweets",['q'=>$query,'count'=>100]);

    foreach($search->statuses as $status){
      // var_dump($status);
      // echo $status->entities->urls[0]->expanded_url . "\n";


      if(isset($status->entities->urls[0]->expanded_url)){
        // echo $status->entities->urls[0]->expanded_url . "\n";
        $site = $status->entities->urls[0]->expanded_url;
        $domain = parse_url($site);

        // echo $site . "\n";

        if(isset($domain['host'])){
          $site = $domain['host'];
        } else {
          $root = $site;
          if($root = extract_site_root($root)){
            $site = $root;
          }
        }
      } else {
        $domain = "twitter.com";
      }



          $args = array(
            'book_id' =>  $book->id,
            'type'  =>  "social",
            'source'  =>  "twitter",
            'source_record_id' => $status->id_str,
            'source_record_time'  =>  date('Y-m-d h:i:s',strtotime($status->created_at)),
            'domain'  =>  $site,
            'url' => "https://twitter.com/i/web/status/" . $status->id_str,
            'excerpt_text'  =>  $status->text,

          );

          // echo substr($status->text,0,115) . "\n";

          var_dump($args);

          $mention_id = check_for_mention($args);


          if(!$mention_id){
            if($mention_id = $db->insert_row('tracker_book_mentions',$args)){
              echo "Mention RECORDED SUCCESSFULLY. \n";
            } else {
              echo "Mention FAILED to record. \n";
              $error->log("Mention FAILED to record. book_id: " . $book->id . ". isbn: " . $isbn . "",array(
                'subject_id'=>$book->id,'subject_type'=>"book_id",'subject_name'=>"inserting mention"
              ));
            }
          } else {

            echo "Mention already stored.\n";
            $args = array(
              'book_id' =>  $book->id,
              'type'  =>  "social",
              'source'  =>  "twitter",
              'source_record_id' => $status->id_str,
              'source_record_time'  =>  date('Y-m-d h:i:s',strtotime($status->created_at)),
              'domain'  =>  $site,
              'url' => "https://twitter.com/i/web/status/" . $status->id_str,
              'excerpt_text'  =>  $status->text,
              'is_archived' =>  0
            );
            // var_dump($args);
            if($db->update_row('tracker_book_mentions',$mention_id,$args)){
              echo "Mention updated successfully.\n";
            }
          }

          // sleep(1);


    }

  }


  sleep(1);

}



  echo "
------------------------------------------
TOTAL records to track: ". count($isbns) ."
------------------------------------------
";

  // $nooch = $isbns;
  // $isbns = array();
  //
  // $i = 20;
  //
  // for($i=100;$i<240;$i++){
  //   $isbns[] = $nooch[$i];
  // }

  // $isbns = array('0998325309'); //blitz
  // $isbns = array('082544392X'); //organic jesus

  // $isbns = array();

    foreach($isbns as $isbn){

      if(isset($_REQUEST['isbn'])){
        if($datum->isbn != $_REQUEST['isbn']){
          // continue;
        }
      }

      if(strlen($isbn) != 10){
        echo "Invalid isbn: Not a 10 character string. \n";
        $error->log("Invalid isbn. Not a 10 character string. ",array(
          'subject_type'=> "isbn",
          'subject_id'=> $isbn,
          'subject_name'=>"item lookup"
        ));
      } else {

        $isbn_id = track_isbn($isbn);
        $record = get_isbn_record($isbn_id);
        echo "Getting info for isbn# " . $isbn . " - ". $record->title ."\n";


          $args = array(
            'isbns'=>array($isbn)
          );

          $book = new Book($args);

          // var_dump($args);

          if(!isset($book->id) || $book->id == "" || !$book->id){
            var_dump($args);
            $args = array(
              'subject_id'  =>  $isbn,
              'subject_type'  =>  "isbn",
              'subject_name' => "instatiate book"
            );
            $message = "failed to get object for book. isbn# " . $isbn;
            $error->log($message,$args);
            echo $message . "\n";
          }

          // var_dump($book);

    $skip = 0;
  //  $query = "Bram Stoker Dracula";
  if(isset($book->authors[0])){
    $author = $book->authors[0];
  } else {
    $author = "";
  }

  $title = $book->title;

  if(strpos($title,':')){
    $parts = explode(":",$title);
    $title = $parts[0];
  }

  if(strpos($title,'(')){
    $parts = explode("(",$title);
    $title = $parts[0];
  }

  // $author = "tim shaw";

    $query = '"'. $title .'"' . " + " . '"'. $author .'"';

    // var_dump($book->authors);

    // $query = "blitz your life tim shaw";

    // $query = trim($query);
    $query = urlencode($query);

      $accountKey = '8e400ca4d02744a5abe51f7b32f13fb1';



      $url =  'https://api.cognitive.microsoft.com/bing/v5.0/search?q='. $query .'&count=100&offset='. $skip .'&mkt=en-us&safesearch=Moderate';

      // Create a stream
      $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Ocp-Apim-Subscription-Key: $accountKey"
        )
      );
      if($bing_callcounter > 1000){
        $bing_logfile = "bing_logfile.txt";
        $fh = fopen($bing_logfile,'a');
        fwrite($fh, date('Y-m-d h:i:s') . " You have attempted to make " . $bing_callcounter . " requests to bing \n. ");
        fclose($fh);
        exit;
      } else {
        fwrite($fh, date('Y-m-d h:i:s') . " Attempting to make call#" . $bing_callcounter . " to bing \n. ");
        sleep(1);
      }
      $bing_callcounter++;
      $context = stream_context_create($opts);

      // Open the file using the HTTP headers set above
      $file = file_get_contents($url, false, $context);

      $urldecode = rawurldecode($file);
      $jsondecode = json_decode($urldecode);
      // var_dump($jsondecode);

       if(isset($jsondecode->webPages->value)){

         $results = $jsondecode->webPages->value;

         $domains = array();

         foreach ($results as $result){

           if(!isset($result->dateLastCrawled)){
            //  continue;
           }

           $site = $result->displayUrl;

           $domain = parse_url($result->displayUrl);

           if(isset($domain['host'])){
             $site = $domain['host'];
           } else {
             $root = $result->displayUrl;
             if($root = extract_site_root($root)){
               $site = $root;
             }
           }

           //echo $site . " \t ". $result->displayUrl ."\n";
           // echo $site ."\n";
           // if($site == "twitter.com"){
           //   if($add = add_domain_to_index($site,"social")){
           //     echo $add . "\n";
           //   } else {
           //     echo "failed \n";
           //   }
           //   var_dump($result);
           //
           //
           // }

           add_domain_to_index($site,"web");

           $url = "";

           $bing_redirect_parts = explode('&r=',$result->url);
           $url_parts = explode('&p=',$bing_redirect_parts[1]);
           $url = $url_parts[0];

           // var_dump($result);

           $domains[] = date('Y-m-d h:i:s',strtotime($result->dateLastCrawled)) . " - " . $url;

           $args = array(
             'book_id' =>  $book->id,
             'type'  =>  "web",
             'source'  =>  "bing",
             'source_record_time'  =>  date('Y-m-d h:i:s',strtotime($result->dateLastCrawled)),
             'domain'  =>  $site,
             'url' => $url,
             'excerpt_text'  => $result->snippet
           );
           $mention_id = check_for_mention($args);
           if(!$mention_id){
             if($mention_id = $db->insert_row('tracker_book_mentions',$args)){
               echo "Mention RECORDED SUCCESSFULLY. \n";
               $error->log("Mention RECORDED SUCCESSFULLY. book_id: " . $book->id . ". isbn: " . $isbn . "",array(
                 'subject_id'=>$book->id,'subject_type'=>"book_id",'subject_name'=>"inserting mention"
               ));
             } else {
               echo "Mention FAILED to record. \n";
               $error->log("Mention FAILED to record. book_id: " . $book->id . ". isbn: " . $isbn . "",array(
                 'subject_id'=>$book->id,'subject_type'=>"book_id",'subject_name'=>"inserting mention"
               ));
             }
           } else {
             echo "Mention already stored.\n";
             $args = array(
               'book_id' =>  $book->id,
               'type'  =>  "web",
               'source'  =>  "bing",
               'source_record_time'  =>  date('Y-m-d h:i:s',strtotime($result->dateLastCrawled)),
               'domain'  =>  $site,
               'url' => $url,
               'excerpt_text'  => $result->snippet,
               'is_archived' => 0
             );
             // var_dump($args);
             if($db->update_row('tracker_book_mentions',$mention_id,$args)){
               echo "Mention updated successfully.\n";
             }
             $error->log("Mention already stored. book_id: " . $book->id . ". isbn: " . $isbn . "",array(
               'subject_id'=>$book->id,'subject_type'=>"book_id",'subject_name'=>"inserting mention"
             ));

           }


           echo "
   -----------------------------------------------------------------------
   ". substr($book->title,0,25) ."\t". $site ."\t". date('Y-m-d',strtotime($result->dateLastCrawled)) ."\t". substr($result->snippet,0,35) ."
   -----------------------------------------------------------------------
           \n";


         }

        //  var_dump($results);


       } else {
         $args = array(
           'subject_id'  =>  $isbn,
           'subject_type'  =>  "isbn",
           'subject_name' => "bing request"
         );
         $message = "No results from bing. ";
         $error->log($message,$args);
       }

       var_dump($domains);
       echo $query;


      //echo "balls\n";


      }


      sleep(1);

      // break;

  }



  echo "success! \n";

  function check_for_mention($args=array()){

  //  echo "SNOOGENS!\n";
  // $sql = "
  //   SELECT * FROM tracker_book_mentions
  //   WHERE book_id = '". $args['book_id'] ."'
  //     AND domain = '". $args['domain'] ."'
  //     AND source_record_time = '". $args['source_record_time'] ."'
  //     AND mention_time > DATE_SUB(CURDATE(), INTERVAL 1 DAY)
  // ";
  $sql = "
    SELECT * FROM tracker_book_mentions
    WHERE book_id = '". $args['book_id'] ."'
      AND domain = '". $args['domain'] ."'
      AND mention_time > DATE_SUB(CURDATE(), INTERVAL 1 DAY)
      AND type = '" . $args['type'] ."'
  ";

    global $db;
//  echo $sql . "\n";
    if($results = $db->get_results($sql)){
      if (count($results)>0){
        return $results[0]->id;
      } else {
        return false;
      //  echo $sql . "\n";
      }
    } else {
    //  echo $sql . "\n";
      return false;
    }
  }

  function add_domain_to_index($domain,$catagory){
    $success = false;
    $found = false;
    $sql = "SELECT id FROM index_source_hosts WHERE domain = '". $domain ."' LIMIT 1";
    global $db;

    if($results = $db->get_results($sql)){
      //return "snoogens";
      if(count($results) > 0){
          $success = "FOUND";
          $found = true;
      }
    }

    if (!$found){
      $sql = "INSERT INTO index_source_hosts (domain,catagory) VALUES ('". $domain ."','". $catagory ."')";
      if($result = $db->query($sql)){
        $success = true;
      }
    }



    if(file_exists($lock_file)){
      $bing_logfile = "bing_logfile.txt";
      $fh = fopen($bing_logfile,'a');
      fwrite($fh, date('Y-m-d h:i:s') . " Script complete. Destroying lock file.  \n. ");
      fclose($fh);
      unlink($lock_file);
    }

    return $success;

    exit;

    // global $db;
    // $sql = "INSERT INTO index_source_hosts (domain,catagory) VALUES ('". $domain ."','". $catagory ."')";
    // $result = $db->query($sql);
  }

  function extract_site_root($str){
    $root = false;
    // $domains = array(".aero",".biz",".cat",".com",".coop",".edu",".gov",".info",".int",".jobs",".mil",".mobi",".museum",
    // ".name",".net",".org",".travel",".ac",".ad",".ae",".af",".ag",".ai",".al",".am",".an",".ao",".aq",".ar",".as",".at",".au",".aw",
    // ".az",".ba",".bb",".bd",".be",".bf",".bg",".bh",".bi",".bj",".bm",".bn",".bo",".br",".bs",".bt",".bv",".bw",".by",".bz",".ca",
    // ".cc",".cd",".cf",".cg",".ch",".ci",".ck",".cl",".cm",".cn",".co",".cr",".cs",".cu",".cv",".cx",".cy",".cz",".de",".dj",".dk",".dm",
    // ".do",".dz",".ec",".ee",".eg",".eh",".er",".es",".et",".eu",".fi",".fj",".fk",".fm",".fo",".fr",".ga",".gb",".gd",".ge",".gf",".gg",".gh",
    // ".gi",".gl",".gm",".gn",".gp",".gq",".gr",".gs",".gt",".gu",".gw",".gy",".hk",".hm",".hn",".hr",".ht",".hu",".id",".ie",".il",".im",
    // ".in",".io",".iq",".ir",".is",".it",".je",".jm",".jo",".jp",".ke",".kg",".kh",".ki",".km",".kn",".kp",".kr",".kw",".ky",".kz",".la",".lb",
    // ".lc",".li",".lk",".lr",".ls",".lt",".lu",".lv",".ly",".ma",".mc",".md",".mg",".mh",".mk",".ml",".mm",".mn",".mo",".mp",".mq",
    // ".mr",".ms",".mt",".mu",".mv",".mw",".mx",".my",".mz",".na",".nc",".ne",".nf",".ng",".ni",".nl",".no",".np",".nr",".nu",
    // ".nz",".om",".pa",".pe",".pf",".pg",".ph",".pk",".pl",".pm",".pn",".pr",".ps",".pt",".pw",".py",".qa",".re",".ro",".ru",".rw",
    // ".sa",".sb",".sc",".sd",".se",".sg",".sh",".si",".sj",".sk",".sl",".sm",".sn",".so",".sr",".st",".su",".sv",".sy",".sz",".tc",".td",".tf",
    // ".tg",".th",".tj",".tk",".tm",".tn",".to",".tp",".tr",".tt",".tv",".tw",".tz",".ua",".ug",".uk",".um",".us",".uy",".uz", ".va",".vc",
    // ".ve",".vg",".vi",".vn",".vu",".wf",".ws",".ye",".yt",".yu",".za",".zm",".zr",".zw");
    $domains = array('/','?');
    foreach ($domains as $domain){
      $parts = explode($domain,$str);
      if(count($parts)>1 && !$root){
        $root = $parts[0];
      }
    }
    if($root){
      $root = str_replace('https://www.','',$root);
      $root = str_replace('https://www','',$root);
      $root = str_replace('https://','',$root);
      $root = str_replace('www.','',$root);
      $root = str_replace('http://www.','',$root);
      $root = str_replace('http://www','',$root);
      $root = str_replace('http://','',$root);
    }
    return $root;
  }


 ?>
