<?php

require_once BUZZTRACE_API_PATH . "interfaces/Amazon.php";
require_once BUZZTRACE_API_PATH . "models/Book.php";
require_once BUZZTRACE_API_PATH . "models/Error.php";

  $error = new Error_log(array(
    'script' => "Sales_Tracker.php",
    'subject_name' => "Amazon",
    'subject_type' => "tracker_script"
  ));


  //this link gets the buzztrace site to spit out a json with all of the isbns that the users are tracking
  $json = file_get_contents(BUZZTRACE_SITE_URL . "wp-content/plugins/buzztrace/src/Admin/api_share_booklist.php");
  $data = json_decode($json);

  global $db;

  foreach($data->isbns as $isbn){
      if ($isbn_id = track_isbn($isbn)){
        $record = get_isbn_record($isbn_id);
        echo $isbn . " inserted successfully - id# " . $isbn_id . " - tracking: ". $record->tracking ."\n";
      } else {

        echo $isbn . " - failed to insert! \n";
        $error->log("Failed to insert isbn. " . $isbn);
      }
  }

  // $json = file_get_contents(BUZZTRACE_BETA_URL . "wp-content/plugins/buzztrace/src/Admin/api_share_booklist.php");
  // $data = json_decode($json);
  //
  // foreach($data->isbns as $isbn){
  //     if ($isbn_id = track_isbn($isbn)){
  //       $record = get_isbn_record($isbn_id);
  //       echo $isbn . " inserted successfully - id# " . $isbn_id . " - tracking: ". $record->tracking ."\n";
  //     } else {
  //       echo $isbn . " - failed to insert! \n";
  //         $error->log("Failed to insert isbn. " . $isbn);
  //     }
  // }


  $isbns = get_isbn_list(1);
  //$isbns = build_isbn_list(1000);
//  $isbns = array("0486411095"); //dracula


  $counter = 0;
  $blocked = 0;

  $isbns = array('0998325309');//blitz

  foreach($isbns as $isbn){

    if(strlen($isbn) != 10){
      echo "Invalid isbn: Not a 10 character string. \n";
      $error->log("Invalid isbn. Not a 10 character string. ",array(
        'subject_type'=> "isbn",
        'subject_id'=> $isbn,
        'subject_name'=>"item lookup"
      ));
    } else {



      $markets = [
          '.com'=>'buzztrace-20',
          '.ca'=>"buzztrace09-20",
          '.co.uk'=>"buzztracecom-21",
        //  '.de'=>"buzztraceco02-21",
        //  '.es'=>"buzztraceco01-21",
          //'.fr'=>"buzztracec011-21",
        //  '.it'=>"buzztraceco0f-21"
      ];

      foreach($markets as $market => $AssociateTag ){
        // $AssociateTag = "buzztrace-20";
        // $market = ".com";


        $isbn_id = track_isbn($isbn);
        $record = get_isbn_record($isbn_id);
        echo "Getting info for isbn# " . $isbn . " - ". $record->title ."\n";

          $book = new Book(array(
            'isbns'=>array($isbn)
          ));

        $args = array(
            'Operation' =>  "ItemLookup",
            'Keywords'  =>  $isbn,
            'AssociateTag'=> $AssociateTag,
            'market'  =>  $market
        );
         $args['SearchIndex'] = "none";
           $amazon = new Amazon_API();
           $amazon->build_request($args);

          if(!$xml = $amazon->get_response($args)){
              $args['SearchIndex'] = "books";
              $amazon = new Amazon_API();
              $amazon->build_request($args);
              $xml = $amazon->get_response($args);
          }

          if(!$xml){
            $error->log("Tried with and without search index. Still Failed. ",array(
              'subject_type'=>"isbn",
              'subject_id'=> $isbn,
              'subject_name'=>"item lookup"
            ));
            echo "Isbn: ". $isbn .".Tried to lookup with and without search index. Still Failed.\n";
            // exit(1);
          }

           $rank = $xml->Items->Item->SalesRank;

          //  var_dump($xml->Items->Item->ItemAttributes->Author);
          //  var_dump($xml->Items->Item);

          // $book->authors = $xml->Items->Item->ItemAttributes->Author;

          $author = (array)$xml->Items->Item->ItemAttributes->Author;
          // var_dump($author);
          $authors = array('authors'=>serialize($author));
          $book->update($authors);
          // var_dump($book->authors);

          //  continue;


           if ($rank > 0){

             $sql = "
              INSERT INTO
              tracker_amazon_salesrank ( isbn, salesrank, market )
              VALUES ('". $isbn ."','". $rank ."','". $market ."')
             ";

             if($db->query($sql)){
               $Item = $xml->Items->Item;
               $Attr = $Item->ItemAttributes;
               $title = $Attr->Title;
               $binding = $Attr->Binding;
               $image_url = $Item->MediumImage->URL;

              //  die();
              //  var_dump($Item->ItemAttributes->Binding);
              //  die('test');
               $args = array(
                 'title'=>$title,
                 'binding'=>$binding,
                 'market'=>$market,
                 'image_url'=>$image_url,
                 'authors'=> $Attr->Author
               );
               $book_args = array(
                 'title'  =>  $title,
                 'image'  =>  $image_url
               );
               $book->update($book_args);
               $isbn_id = isbn_exists($isbn);
               $db->update_row('list_isbn',$isbn_id,$args);
              // var_dump($Item);

               //echo $xml->Items->Item->ItemAttributes->Title . " (" . $rank . ") \n";
               echo substr($Item->ItemAttributes->Title,0,15) . " - #". $isbn ." inserted and tracked." . "\n";

                 foreach ($Item->AlternateVersions->AlternateVersion as $version){
                  // echo "version: ";
                  // var_dump($version);
                   if($book->add_isbn_to_list($version->ASIN)){
                     echo "isbn: " . $version->ASIN . " added to book object. \n";
                   } else {
                     echo "FAILED TO ADD ISBN: " . $version->ASIN . " to book object. \n";
                     //$error->log("FAILED TO ADD ISBN: " . $version->ASIN . " to book object.");
                   }
                   $args = array(
                     'title'=>$version->Title,
                     'binding'=>$version->Binding,
                     'market'=>$market
                   );
                   if ($isbn_id = isbn_is_tracked($version->ASIN)){
                     $db->update_row('list_isbn',$isbn_id,$args);
                     echo "alt: " . $version->ASIN . " updated, still tracking.\n";
                   } elseif ($isbn_id = isbn_exists($version->ASIN)) {
                     $args['tracking'] = 0;
                     $db->update_row('list_isbn',$isbn_id,$args);
                     echo "alt: " . $version->ASIN . " updated, still not tracking.";
                   } else {
                     $args['tracking'] = 0;
                     $isbn_id = add_isbn_to_list($version->ASIN);
                     $db->update_row('list_isbn',$isbn_id,$args);
                     echo "alt: " . $version->ASIN . " inserted, not tracked.\n";
                   }
                 }


             } else {
               echo $isbn . " failed to write. \n";
                 $error->log("Query failed to insert new isbn: " . $isbn . "  SQL: " . $sql);
             }

             if($book->last_amazon_review($market) < (time() - ( 24 * 60 * 60 ))){



                         $url = "https://www.amazon". $market ."/dp/" . $isbn;

                         if ($source = file($url)){
                           $test_test = false;
                           $reviews_test = false;
                           foreach($source as $line){
                               $test = $line;
                               $test = trim($test);
                               $test = str_split($test);
                               if(count($test) > 0){
                                 $test_test = true;
                               }
                               //as soon as we find 404 break loop and return false
                               if (strpos($line, '<title>404')){
                                   $data = false;
                                   echo "ERROR: 404 found. \n";
                                   break 2;
                               }
                               //get reviews
                               if($string = strpos($line,'customer reviews')){
                                 $line = explode(' customer reviews', $line);

                                   $span = explode('>',$line[0]);
                                 if(count($span) > 1 ){
                                   $total = str_replace(',','',$span[1]);
                                   if (is_numeric($total)){
                                     $sql = "INSERT INTO tracker_book_reviews
                                     (domain,url,total,book_id,content_hash) values
                                     ('www.amazon". $market ."','". $url ."','". $total ."','". $book->id ."','". md5(implode($source)) ."')
                                     ";
                                     if($db->query($sql)){
                                       echo "Review total recorded. \n";
                                     } else {
                                       $error->log("Query failed: " . $sql,array(
                                         'subject_type'=>"insert query",
                                         'subject_name'=>"scraping customer review"
                                       ));
                                       echo $sql . '\n';
                                     }
                                       echo "Total Reviews: " . $total . "\n";
                                   }
                                 }
                                 $reviews_test = true;
                               }
                           }
                           if (!$reviews_test){
                             echo "BLOCKED!\n";
                             $blocked++;
                           } else {
                             $counter++;
                           }
                         } else {
                           echo "ERROR: failed to open page. \n";
                             $error->log("Amazon blocked us from scraping. " . $isbn);
                         }



             } else {
               echo "ALREADY TRACKED this review in the past 24 hours. \n";
               $counter++;
             }



           } else {
               $error->log("No rank found through api. " . $isbn);
             echo "something effed up. \n";
           }

           echo "Counted: \t" . $counter . "\t Blocked: \t" . $blocked . ".\n";

           sleep(1);


      }



    }

  }

  echo "success! \n";


 ?>
