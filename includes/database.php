<?php

class Database {

  var $host = "localhost";
  var $user = "buzztrace_api";
  var $password = "ydpSuogwQn6f9OtwSrPAfIWNCD2tC7EL";
  var $database = "buzztrace_bigdata";
  var $link;

  function __construct(){

    if(isset($args)){
      if($args->host){
        $this->host = $args->host;
      }
      if($args->user){
        $this->user = $args->user;
      }
      if($args->password){
        $this->password = $args->password;
      }
      if($args->database){
        $this->database = $args->database;
      }
    }
    $this->link = mysqli_connect($this->host,$this->user,$this->password,$this->database);
  }


  function query($sql){
    $result = mysqli_query($this->link,$sql);
    return $result;
  }

  function get_results($sql,$return_type=OBJ){
    $result = array();
    if ($query = mysqli_query($this->link,$sql)){
        if($return_type == OBJ){
          while($row = mysqli_fetch_object($query)){
            $result[] = $row;
          }
        }
        if($return_type == ARR){
          while($row = mysqli_fetch_assoc($query)){
            $result[] = $row;
          }
        }
    }
    return $result;
  }

  function update_row($table,$id,$args){
    //var_dump($args);
    if(!is_array($args)){
      $args = unserialize($args);
    }
  //  die($id);
    $updates = array();

    foreach ($args as $column => $value){
      if(is_array($value)){
        $value = serialize($value);
      } else {
        $value = addslashes($value);
      }
      $updates[] = $column . " = '". $value ."'";
    }
    $sql = "UPDATE `". $table ."` SET " . implode(',',$updates);
    $sql .= " WHERE id = " . $id . " LIMIT 1";
    //echo $sql;
    return $this->query($sql);
  }

  function insert_row($table,$args){
    $insert_id = false;
    $columns = array();
    $values = array();
    foreach ($args as $column => $value){
      if(is_array($value)){
        $value = serialize($value);
      } else {
        $value = addslashes($value);
      }
      $columns[] = $column;
      $values[] = "'" . addslashes($value) . "'";
    }

    $sql = "INSERT INTO `". $table ."` (". implode(',',$columns) .") VALUES (". implode(',',$values) .")";

  //  echo $sql . "\n";

    if($this->query($sql)){
      $insert_id = mysqli_insert_id($this->link);
    } else {
      echo $sql . "\n";
    }

    return $insert_id;

  }

}

class Buzztrace extends Database {




}


?>
