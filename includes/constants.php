<?php

  //site paths
  define('BUZZTRACE_API_VERSION', "v1");
  define('BUZZTRACE_API_PATH', "/var/www/vhosts/api.buzztrace.com/api/" . BUZZTRACE_API_VERSION . "/");
  define('BUZZTRACE_API_URL', "http://api.buzztrace.com/" . BUZZTRACE_API_VERSION . "/");

  define('BUZZTRACE_DEV_URL','http://dev.buzztrace.com/');
  define('BUZZTRACE_BETA_URL','http://beta.buzztrace.com/');
  define('BUZZTRACE_LIVE_URL','https://www.buzztrace.com/');
  define('BUZZTRACE_SITE_URL','https://www.buzztrace.com/');
  define('BUZZTRACE_STAGING_URL','http://staging.buzztrace.com/');


  //time periods in seconds
  define('SECOND',1);
  define('MINUTE',SECOND * 60);
  define('HOUR',MINUTE * 60);
  define('DAY',(HOUR * 24));
  define('WEEK', DAY * 7);
  define('MONTH',WEEK * 4);
  define('YEAR',(DAY * 365) + (HOUR * 6));

  //shorthand
  define('OBJ', "object");
  define('ARR', "array");


?>
