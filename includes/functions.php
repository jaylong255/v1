<?php

  function build_isbn_list($threshold = 100){
    $c = 0;
    $isbns = get_isbn_list(1);
    $c = count($isbns);
    $untracked = get_isbn_list(0);
    for($i=0;$i<$threshold-$c;$i++){
      $isbns[] = $untracked[$i];
    }
    return $isbns;
  }

  function get_isbn_list($tracking = 1,$args=array()){
    $isbns = array();
    global $db;
    $sql = "SELECT isbn FROM list_isbn WHERE tracking = " . $tracking;
    $res = $db->get_results($sql);

    if(count($res) > 0){
      foreach($res as $row){
        $isbns[] = $row->isbn;
      }
    }
    return $isbns;
  }

  function add_isbn_to_list($isbn,$args=array()){
    global $db;
    $sql = "INSERT INTO list_isbn (isbn) VALUES ('". addslashes($isbn) ."')";
    if(count($args)>0){
      if (count($args > 0)){
        $columns = array();
        $values = array();
        foreach($args as $column => $value){
          $columns[] = $column;
          $values[] = "'" . addslashes($value) . "'";
        }
        $sql = "INSERT INTO list_isbn (isbn, ". implode(',',$columns) .") VALUES ('". $isbn ."', ". implode(',',$values) .")";
      }
    }
    $insert = $db->query($sql);
    return mysqli_insert_id($db->link);
  }

  function isbn_exists($isbn){
    global $db;
    $sql = "SELECT * FROM list_isbn WHERE isbn = '". $isbn ."'";
    $results = $db->get_results($sql);
    //var_dump($results);
    if(count($results) > 0){
      $isbn_id = false;
      foreach ($results as $result){
        $isbn_id = $result->id;
      }
      return $isbn_id;
    } else {
      return false;
    }
  }

  function isbn_is_tracked($isbn){
    global $db;
    $sql = "SELECT tracking FROM list_isbn WHERE isbn = '". $isbn ."'";
    if($results = $db->get_results($sql)){
      $tracked = 0;
      foreach ($results as $result){
        $tracked = $result->tracking;
      }
      return $tracked;
    } else {
      return false;
    }
  }

  function track_isbn($isbn){
    $isbn_id = false;
    global $db;
    if($isbn_id = isbn_exists($isbn)){
      $args = array('tracking'=>1);
      $db->update_row('list_isbn',$isbn_id,$args);
    } else {
      $isbn_id = add_isbn_to_list($isbn,array('tracking'=>1));
    }
    return $isbn_id;
  }

  function get_isbn_record($isbn_id){
    global $db;
    $sql = "SELECT * FROM list_isbn WHERE id = " . $isbn_id;
    $record = false;
    $results = $db->get_results($sql);
    foreach($results as $result){
      $record = $result;
    }
    return $record;
  }

 ?>
