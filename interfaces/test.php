<?php

   /*

       this class is meant to be used to retrieve records from the buzztrace database and to update them from scraping amazon stores from various world markets. get your book id's handy, this all hinges on isbn and asin

   */

   //this is the current convention for requesting http from amazon that contains the data we are looking for the isbn is interchangeable with the asin.

//         '.co.uk'   =>  "https://www.amazon.co.uk/dp/" . $isbn,
//         '.de'   =>  "https://www.amazon.de/dp/" . $isbn,
//         '.cn'   =>  "https://www.amazon.cn/dp/" . $isbn,
//         '.co.jp'   =>  "https://www.amazon.co.jp/dp/" . $isbn,
//         '.es'   =>  "https://www.amazon.es/dp/" . $isbn,
//         '.fr'   =>  "https://www.amazon.fr/dp/" . $isbn,
//         '.it'   =>  "https://www.amazon.it/dp/" . $isbn,
//         '.in'   =>  "https://www.amazon.in/dp/" . $isbn,
//         '.ca'   =>  "https://www.amazon.ca/dp/" . $isbn,
//         '.au'   =>  "https://www.amazon.au/dp/" . $isbn


/*$releases = array(

   $release = {
       sales rank. developing code to read amazon page sources by isbn# for sales rank data
       'id' : (isbn or asin)
       'market' : (.com, .cn, .co.uk, etc...)

   }

);
*/

   /*

           //first we want to check our database. if we have records less than an hour old, there's no need to initiate a scrape.

           $sql = "

               SELECT
                   buzztrace_books.ID AS id,
                   buzztrace_books.ISO_ID AS iso_id
                   buzztrace_books.book_title AS title,
                   buzztrace_books.book_market AS market,
                   buzztrace_ranks.rank_datetime AS rank_time,
                   buzztrace_ranks.rank AS rank
               FROM buzztrace_books
                   JOIN buzztrace_ranks ON buzztrace_books.ID = buzztrace_ranks.book_id
               WHERE buzztrace_books.ISO_ID = '". $id ."'
                   ORDER BY buzztrace_ranks.rank_time DESC

           ";


           //then check for parent and child releases

           //if any of the other releases with that iso_id are found, we need to update our parent id. if another parent_id is found, use it. if not, it should just be the one other release, we're going to use that book's id as our parent_id



           if ($select = $wpdb->get_result($sql)){
               foreach ($select AS $rank){
                   if (strtotime(date('Y-m-d h:i:s')) - strtotime($rank->rank_time) > 3600){

                       //crawl this release
                       foreach ($this->markets as $market){

                           if($data = source_data($id,$market)){
                               $args = array('book_ID'=>$rank->book_ID,'rank_datetime'=>date('Y-m-d h:i:s'),'rank'=>$data->rank);
                               $wpdb->insert('buzztrace_ranks',$args);
                           }

                       }

                   }
               }
           } else {
               $args = array('ISO_ID'=>$id,);
               $wpdb->insert('buzztrace_books',$args);
           }

   */

   /*MARKET IS CURRENTLY HARDCODED TO .COM (insert_release method)*/

//    class Book {
//
//        // var $title;
//
//       //  var $markets = array('.com','.co.uk','.de','.cn','.co.jp','.es','.fr','.it','.in','.ca','.au');
//
//        public $markets = array('.com');
//        public $releases = array();
//        public $ranks = array();
//
//
//         public function select_release($buzztrace_id){
//
//             //selects a particular release by buzztrace_id, returns false on failure
//
//             $release = false;
//
//              global $wpdb;
//
//             $sql = "
//
//                 SELECT
//                     ID AS buzztrace_id,
//                     IS0_ID AS iso_id,
//                     book_title AS title,
//                     book_author AS author,
//                     book_parent AS parent_id,
//                     book_market AS market
//
//                 FROM buzztrace_books WHERE ID = '". $buzztrace_id ."'
//
//
//             ";
//
// //             if ($res = $wpdb->get_results($sql)){
// //                 if(count($res)>0){
// //                     $release = $res[0];
// //                 }
// //             }
//
//             return $release;
//         }
//
//
// //         $release = { id, market, ranks[] }
// //         $rank{ date, rank }
//
//         public function insert_release($iso_id,$title,$args){
//
//             $market = ".com"; //ultimately we want to look through, verify and srape if necessary, all markets. this is hardcoded.
//
//             //so we don't break our foreach if $args is empty
//             if (!$args){ $args = array(); }
//
//             $buzztrace_id = false;
//
//             //first check for existing releases with this osi_id and market
//             if($releases = select_releases($iso_id,$market)){
//
//                 //if one is found, return the buzztrace_id
//                 $buzztrace_id = $releases[0]->buzztrace_id;
//
//                 if($args['book_parent']){
//
//                     $wpdb->update('buzztrace_books',array('book_parent'=>$args['book_parent']),array('ID'=> $buzztrace_id));
//
//                 }
//
//             } else {
//
//                 //insert new release. iso, market and title are required
//                 $data = array(
//                     'ISO_ID' => $iso_id,
//                     'book_market' =>    $market,
//                     'book_title' => $title
//                 );
//
//                 //any additional fields may be passed into the args array. must follow table column naming conventions
//                 foreach($args as $key=>$value){
//                     $data[$key] = $value;
//                 }
//
//                 if ($wpdb->insert('buzztrace_books',$data)){
//                     $buzztrace_id = $wpdb->insert_id;
//                 }
//
//             }
//
//             return $buzztrace_id;
//
//         }
//
//
//
//        public function select_releases($iso_id,$market){
//
//             //market not required
//
//             $releases = false;
//
//             global $wpdb;
//
//             //build an array of releases with this id
//
//             $sql = "
//
//                 SELECT
//                     ID AS buzztrace_id,
//                     IS0_ID AS iso_id,
//                     book_parent AS parent_id,
//                     book_market AS market
//
//                 FROM buzztrace_books WHERE ISO_ID = '". $iso_id ."'
//
//
//             ";
//
//             if ($market){
//
//                 $sql .= "
//
//                     AND book_market = '". $market ."'
//
//                 ";
//
//             }
//
//             if ($res = $wpdb->get_results($sql)){
//                 if (count($res)>0){
//                     $releases = $res;
//                 } else {
//                     $releases = false;
//                 }
//             } else {
//                 $releases = false;
//             }
//
//             //now find any parent or child releases associated with this book
//
//             $sql = "
//
//                 SELECT
//                     ID AS buzztrace_id,
//                     IS0_ID AS iso_id,
//                     book_parent AS parent_id,
//                     book_market AS market
//
//                 FROM buzztrace_books WHERE ( 1 = 0 )
//
//             ";
//
//             foreach ($releases as $release){
//                 $sql .= "
//
//                     OR (book_parent = '". $release->parent_id ."')
//                     OR (book_parent = '". $release->buzztrace_id ."')
//
//                 ";
//             }
//
//             if ($res = $wpdb->get_results($sql)){
//
//                 foreach($res as $release){
//
//                     $releases[] = $release;
//
//                 }
//
//             }
//
//             return $releases;
//
//         }
//
//         /*
//
//         function select_ranks($release_id){
//
//             global $wpdb;
//
//
//
//         }
//
//         function update_releases($osi_id,$data){
//
//             /*
//
//                 $releases = [all amazon scrape data];
//
//             */
//
// //         }
// //
// //         function scrape_amazon($osi_id,$releases){
// //
// //             //  https://www.amazon.com/dp/1338099132 (harry potter isbn usa and others)
// //             //  https://www.amazon{market}/dp/{id}
// //
// //             foreach ($this->markets as $market){
// //
// //
// //
// //             }
// //
// //
// //
// //         }
// //
// //         function scrape_release($osi_id,$market){
// //
// //             //input osi_id and market, returns array of all scrapable data or false on failure
// //
// //             //$data = { id,market,title,rank }
// //
// //             $data = false;
// //
// //
// //             if($source = file("https://www.amazon". $market ."/dp/" . $osi_id)){
// //
// //                 foreach($source as $line){
// //
// //                     //as soon as we find 404 break loop and return false
// //                     if ($strpos($line, '<title>404')){
// //
// //                         $data = false;
// //                         break 2;
// //
// //                     }
// //
// //                     //get title of the book
// //                     if (strpos($line,'id="productTitle"')){
// //                         $parts = explode('">',$line);
// //                         $title = str_replace('</span>','',$parts[1]);
// //                         $data->title = $title;
// //                     }
// //
// //                     //get current sales rank
// //                     if (strpos($line,">See Top 100")){
// //
// //                         $parts = explode(' ',$line);
// //                         $rank = $parts[0];
// //                         $rank = str_replace('#','',$rank);
// //                         $rank = str_replace(',','',$rank);
// //                         $rank = trim($rank);
// //
// //                         if (is_numeric($rank)){
// //                             $data->rank = number_format($rank);
// //                         }
// //
// //                     }
// //
// //                 }
// //
// //                 if ($data){
// //                     $data->osi_id = $osi_id;
// //                     $data->market = $market;
// //                 }
// //
// //             }
// //
// //             return $data;
// //
// //         }
//
//
//
//     }
//

/*
   class Book {

      // var $title;

      public $id,$title,$author,$cover;
      public $authors = array();

     //  var $markets = array('.com','.co.uk','.de','.cn','.co.jp','.es','.fr','.it','.in','.ca','.au');

      public $markets = array('.com');
      public $releases = array();
      public $ranks = array();


       function __construct($book_id){

           //first check buzztrace to see if we have a record
           if ($book_id){
               $this->id = $book_id;
           }

//             if ($this->check_buzztrace()){
//
//             }

       }

       //checks buzztrace for book records (basic info only, more of a check than a full select)
       public function check_buzztrace($id){

           if ($isbn = get_author_bookmeta($id,'isbn')){

           }

           $book_id = false;

           if (!$id){
               $id = $this->id;

           }

           global $wpdb;

//             $sql = "
//                 SELECT
//                     id,
//                     author_name AS author,
//                     book_name AS title,
//                     book_cover_url AS cover
//                 FROM buzztrace_author_books WHERE id = '". $id ."'
//             ";



           if ($results = $wpdb->get_results($sql)){
               $book_id = $results[0]->book_parent;
           }

           return $book_id;

       }

       //selects basic book info and all releases, no ranks
       public function get_releases(){

           global $wpdb;

           $sql = "

               SELECT
                    buzztrace_book_releases.ID AS id,
                    buzztrace_book_releases.ISO_ID AS iso_id,
                    buzztrace_book_releases.book_market AS market
                    buzztrace_book_releases.binding AS binding
               FROM
                   buzztrace_author_books JOIN buzztrace_book_releases ON buzztrace_author_books.ID = book_id
               WHERE buzztrace_author_books.ID = '". $this->id ."'

           ";

           if ($releases = $wpdb->get_results($sql)){
               foreach ($releases as $release){
                   $this->releases[$release->id] = $release;
               }
           }

       }

       //if release id is specified, we get just that release's rank records, otherwise we loop through and get them all
       public function get_ranks($release_id){

           $releases = array();

           if ($release_id){
               $releases[] = $release_id;
           } else {
               foreach ($this->releases as $release){
                   $releases[] = $release;
               }
           }

           $sql = "
               SELECT
                   rank_ID AS id,
                   release_ID AS release_id,
                   rank_datetime AS rank_time,
                   rank AS rank
               FROM
                   buzztrace_book_ranks WHERE 0
           ";

           foreach ($releases as $release){
               $sql .= " OR release_ID = '". $release->id ."'";
           }

           global $wpdb;

           if ($ranks = $wpdb->get_results($sql)){
               foreach ($ranks as $rank){
                   $this->releases[$rank->release_id]->ranks[$rank->rank_time] = $rank;
               }
           }

       }

   }*/
   /*
   class Release {

       public $id,$iso_id,$parent_id,$binding;
       public $market = ".com";
       public $markets = array();
       public $ranks = array();

       function __construct($iso_id){

           $this->iso_id = $iso_id;

       }

       function check_buzztrace_for_release(){

           $release_id = false;

           $sql = "

               SELECT
                   wp_posts.ID AS release_id,
                   wp_posts.post_parent AS parent_id,
                   wp_posts.post_title AS binding
               FROM wp_posts JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
               WHERE wp_postmeta.meta_key = 'isbn' AND wp_postmeta.meta_value = '". $this->iso_id ."'

           ";

           global $wpdb;

           if ($results = $wpdb->get_results($sql)){

               $release_id = $results[0]->release_id;
               $this->parent_id = $results[0]->parent_id;
               $this->binding = $results[0]->binding;

           }

           return $release_id;

       }

       function get_release_from_buzztrace($release_id){

           $sql = "

               SELECT
                   wp_posts.post_title AS title

               FROM wp_posts WHERE ID = '". $this->parent_id ."'

           ";

           global $wpdb;

           if ($results = $wpdb->get_results($sql)){

               $this->title = $results[0]->title;

           }

       }

       function create_book_in_buzztrace(){

           global $wpdb;

           $defaults = array(
               'post_author' => get_current_user(),
               'post_content' => '',
               'post_content_filtered' => '',
               'post_title' => $this->title,
               'post_excerpt' => '',
               'post_status' => 'publish',
               'post_type' => 'buzztrace_book',
               'comment_status' => '',
               'ping_status' => '',
               'post_password' => '',
               'to_ping' =>  '',
               'pinged' => '',
               'post_parent' => 0,
               'menu_order' => 0,
               'guid' => '',
               'import_id' => 0,
               'context' => '',
           );

           wp_insert_post($defaults);

           $defaults['post_parent'] = $wpdb->insert_id;
           $defaults['post_title'] = $this->binding;

           wp_insert_post($defaults);
           add_post_meta($wpdb->insert_id,'isbn',$this->iso_id);



       }


   }

   */





   //this is the amazon api class. it is defaulted to books. must enter operation and keyword/

   class Amazon_API {

   //  public $AWSSecretKey = "Ko7io+lrwfpDvpk4MhLagdTpMfK+hJUrcsUMRJPz";
     public $AWSSecretKey = "+3tu3TAHz0HBkxCgRfKv6+v37mWRWTrZ12v7eSlA";

       public $url = "http://webservices.amazon.com/onca/xml";
       public $Service = "AWSECommerceService";
       public $AssociateTag = "buzztrace-20"; //us
      // public $AssociateTag = "buzztracecom-21"; //uk
      // public $AssociateTag = "buzztraceco02-21"; //de

      //public $AWSAccessKeyId = "AKIAJVLFJ7YXVC37HTIQ";
      public $AWSAccessKeyId = "AKIAIIWZFRCD3BAFZ6LQ";

       public $Operation = "ItemSearch";
       public $Version = "2013-08-01";
       public $SearchIndex = "Books";
       public $Keywords = "";
       public $ItemId = "";
       public $Signature = "";
       public $ResponseGroup = "ItemAttributes,SalesRank,BrowseNodes,Images";
       public $debug = "|nothing";
       public $market = ".com";


       public $request = "no-request";

       function __construct($Operation="",$Keywords="",$args=array()){

           if(isset($Operationa)){
               $this->Operation = $Operation;
           }

           if (isset($SearchIndex)){
               $this->SearchIndex = $SearchIndex;
           }

           if (isset($Keywords)){
               $this->Keywords = $Keywords;
               $this->ItemId = $Keywords;


           }

           foreach ($args as $key => $value){
               if ($value != ""){
                   $this->{$key} = $value;
               }
           }

          // $this->build_request();

       }

       public function build_request($args){

           $request = "test";

           if ($args){

               foreach ($args as $key => $value){
                   if ($value != ""){
                       $this->{$key} = $value;
                       $this->debug .= "|".$value;
                   }
               }
           }

           $this->debug .= "|test";

           if ($this->Operation == "ItemLookup"){

               if (!$this->ItemId){
                   $this->ItemId = $this->Keywords;
               }

               if ($this->SearchIndex == "none" || $this->SearchIndex == "dontuseasearchindex"){
                 $request =
                     "Operation=" . $this->Operation .
                     "&ResponseGroup=" . $this->ResponseGroup .
                     "&Version=" . $this->Version .
                     "&ItemId=" . $this->ItemId .
                     "&AssociateTag=" . $this->AssociateTag;
               } else {
                 $request =
                     "Operation=" . $this->Operation .
                     "&ResponseGroup=" . $this->ResponseGroup .
                     "&Version=" . $this->Version .
                     "&ItemId=" . $this->ItemId .
                     "&SearchIndex=" . $this->SearchIndex .
                     "&AssociateTag=" . $this->AssociateTag;
               }

           } else {

               $request =
                   "Operation=" . $this->Operation .
                   "&ResponseGroup=" . $this->ResponseGroup .
                   "&Marketplace=us" .
                   "&Version=" . $this->Version .
                   "&SearchIndex=" . $this->SearchIndex .
                   "&Keywords=" . $this->Keywords .
                   "&AssociateTag=" . $this->AssociateTag;

           }


           $request =
               'Service=' . $this->Service . '&'.
               'AWSAccessKeyId='. $this->AWSAccessKeyId .'&'.
                   'Timestamp='.gmdate("Y-m-d\TH:i:s\Z").'&'.
                   $request;

           $request = str_replace(',','%2C', $request);
           $request = str_replace(':','%3A', $request);

           //break request string into key/value pairs,
           $reqarr = explode('&',$request);

           //sort on byte value
           sort($reqarr);

           // tie back together w/ &'s
           $string_to_sign = implode("&", $reqarr);

           $string_to_sign = "GET\nwebservices.amazon". $this->market ."\n/onca/xml\n".$string_to_sign;

           //Substitute your real Secret Key here...
           $signature = urlencode(base64_encode(hash_hmac("sha256", $string_to_sign, $this->AWSSecretKey, True)));

           $request .= '&Signature='.$signature;
           $request = 'http://webservices.amazon'. $this->market .'/onca/xml?'.$request;

           $this->request = $request;

          // return $this->request;

       }

       public function get_response($args){

           $parsed_xml = false;

           $Operation = "";
           $Keywords = "";

           if ($this->Operation){
               $Operation = $this->Operation;
           }

           if(isset($args['Operation'])){
               $Operation = $args['Operation'];
           }

           if ($this->Keywords){
               $Keywords = $this->Keywords;
           }

           if(isset($args['Keywords'])){
               $Keywords = $args['Keywords'];
           }

           $this->debug = $Operation . "|" . $Keywords;

           if(($Operation != "") && ($Keywords != "")){

               $args = array(
                   'Operation' =>  $Operation,
                   'Keywords'  =>  $Keywords
               );

               $this->build_request($args);

               $response = file_get_contents($this->request);
               $parsed_xml = simplexml_load_string($response);

           }



           return $parsed_xml;

       }

   }

//     class Book {
//
//         public $book_id,$title,$author,$cover,$releases;
//
//         function __construct($args){
//
// //             //if book id is passed on construct, grab the book
// //             if ($args['book_id']){
// //                 get_book_by_id($args['book_id']);
// //             } elseif(is_numeric(intval($args))){
// //                 get_book_by_id($args);
// //             }
// //
// //             if ($this->book_id){
// //                 $this->get_releases();
// //             }
//
//         }
//
//         function get_book_by_id($book_id){
//
//             $book = false;
//
//             global $wpdb;
//
//             $sql = "
//                 SELECT
//                     wp_posts.ID AS book_id,
//                     wp_posts.post_title AS title
//                 FROM wp_posts
//                 WHERE ID = '". $book_id ."'
//             ";
//             if ($results = $wpdb->get_results($sql)){
//                 $book = $results[0];
//                 $this->book_id = $book->book_id;
//                 $this->title = $book->title;
//             }
//
//             return $book;
//
//         }
//
//     }

//     class Release extends Book {
//
//         public $release_id,$isbn,$binding,$market,$ranks;
//
//
//         function __construct($args){
//
// //             $release_id = false;
// //             $isbn = false;
// //
// //             if ($args['release_id']){
// //                 $this->release_id = $args['release_id'];
// //             } elseif(is_numeric(intval($args))){
// //                 $this->release_id = $args;
// //             }
// //
// //             if ($args['isbn']){
// //                 $this->isbn = $args['isbn'];
// //             }
// //
// //             if ($this->release_id){
// //                 get_release_from_id($this->release_id);
// //             } elseif(){
// //
// //             }
//
//         }
//
// //         function get_release_from_id($release_id){
// //
// //             $release = false;
// //
// //             global $wpdb;
// //
// //             $sql = "
// //                 SELECT
// //                     ID,post_parent AS book_id
// //                 FROM wp_posts
// //                 WHERE ID = '". $release_id ."'
// //             ";
// //
// //             if($results = $wpdb->get_results($sql)){
// //                 $release = $results[0];
// //                 $this->release_id = $release->ID;
// //                 $this->isbn = get_post_meta($this->release_id,'isbn');
// //             }
// //
// //             return $release;
// //
// //         }
// //
// //         function get_release_from_isbn($isbn){
// //
// //             $release = false;
// //
// //             global $wpdb;
// //
// //             $sql = "
// //                 SELECT
// //                     wp_posts.ID AS release_id,
// //                     wp_posts.post_parent AS book_id
// //                 FROM wp_posts
// //                     JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
// //                     WHERE ( wp_postmeta.meta_key = 'isbn' AND wp_postmeta.meta_value = '". $isbn ."' )
// //             ";
// //
// //             if($results = $wpdb->get_results($sql)){
// //                 $release = $results[0];
// //                 $this->book_id = $release->book_id;
// //                 $this->release_id = $release->release_id;
// //                 $this->isbn = $isbn;
// //             }
// //
// //             return $release;
// //
// //         }
// //
// //         function create_release($isbn,$args){
// //
// //             $release = false;
// //
// //
// //             $amazon = new Amazon_API();
// //
// //             $args = array(
// //                 'Operation' =>  "ItemLookup",
// //                 'Keywords'  =>  $isbn
// //             );
// //
// //             if ($xml = $amazon->get_response($args)){
// //                 $Item = $xml->Items->Item;
// //                 $rank = $Item->SalesRank;
// //
// //                 $Item = $Item->ItemAttributes;
// //                 $this->title = $Item->Title;
// //                 $this->author = $Item->Author;
// //                 $this->binding = $Item->Binding;
// //                 $this->market = ".com";
// //
// //                 global $wpdb;
// //
// //                 $book_id = 0;
// //
// //                 if ($args['book_id']){
// //                     $book_id = $args['book_id'];
// //                 }
// //
// //                 if ($this->book_id){
// //                     $book_id = $this->book_id;
// //                 }
// //
// //                 $data = array(
// //                     'post_title'    =>  $this->title,
// //                     'post_type' =>  "buzztrace_book",
// //                     'post_parent'   =>  $book_id
// //                 );
// //
// //                 if ($wpdb->insert('wp_posts',$data)){
// //                     if ($book_id == 0){
// //
// //                         //this book is the parent. update post parent and initiate a new release create
// //                         $book_id = $wpdb->insert_id;
// //                         if($book = $this->get_book_by_id($book_id)){
// //                             $data['post_parent'] = $book_id;
// //                             $where = array('ID'=>$book_id);
// //                             if ($wpdb->update('wp_posts',$data,$where)){
// //                                 $data['post_title'] =>  $this->binding;
// //                                 if($wpdb->insert('wp_posts',$data)){
// //                                     $release_id = $wpdb->insert_id;
// //                                     $this->release_id = $release_id;
// //                                     add_post_meta($release_id,'isbn',$isbn);
// //                                     add_post_meta($release_id,'market',$this->market);
// //                                     add_post_meta($release_id,'author',$this->author);
// //                                     $release = get_release_from_id($release_id);
// //                                 }
// //                             }
// //                         }
// //
// //
// //                     } else {
// //
// //                         //this book is not the parent. just add the row and meta
// //                         if($book = $this->get_book_by_id($book_id)){
// //                             $data['post_parent'] = $book_id;
// //                             $where = array('ID'=>$book_id);
// //                             if ($wpdb->update('wp_posts',$data,$where)){
// //                                 $data['post_title'] =>  $this->binding;
// //                                 if($wpdb->insert('wp_posts',$data)){
// //                                     $release_id = $wpdb->insert_id;
// //                                     $this->release_id = $release_id;
// //                                     add_post_meta($release_id,'isbn',$isbn);
// //                                     add_post_meta($release_id,'market',$this->market);
// //                                     add_post_meta($release_id,'author',$this->author);
// //                                     $release = get_release_from_id($release_id);
// //                                 }
// //                             }
// //                         }
// //
// //                     }
// //                 }
// //
// //             }
// //
// //             return $release;
// //
// //         }
//
//     }


   /*

       credentials for amazon uk


       AssociateTag: buzztracecom-21


       .de,

               buzztraceco02-21
===================================

       .es
               buzztraceco01-21
=====================================

       .fr
         buzztracec011-21
===================================
       .it
               buzztraceco0f-21
====================================






   */



?>
