<?php

    require_once BUZZTRACE_API_PATH.'third_party/facebook/vendor/autoload.php';

    class facebook {

        var $fb,$token,$id,$name,$email,$profile_img,$dump,$link;


        function __construct(){

            $this->fb = new Facebook\Facebook([
                'app_id' => get_field('facebook_api_key','option'),
                'app_secret' => get_field('facebook_api_secret','option'),
                'default_graph_version' => 'v2.6'
            ]);


        }

        // function get_user_token($user_id){
        //
        //     $this->token = false;
        //
        //     $meta = get_user_meta($user_id);
        //
        //     if (isset($meta['fb_access_token'][0])){
        //         $this->token = $meta['fb_access_token'][0];
        //         $this->fb->setDefaultAccessToken($this->token);
        //     }
        //
        //     return $this->token;
        //
        // }

        function set_token($token){

            $this->token = $token;
            $this->fb->setDefaultAccessToken($this->token);
            $this->get_me();

        }

        function get_me(){

            $request = $this->fb->request('GET', '/me?fields=id,name,email,picture,link');
            $response = $this->fb->getClient()->sendRequest($request);
            //$likes = json_decode($response->getBody())->data;
            $data = json_decode($response->getBody());

            $this->id = $data->id;
            $this->name = $data->name;
            $this->email = $data->email;
            $this->profile_img = $data->picture->data->url;
            $this->link = $data->link;
            $this->dump = $data;

            return $response;

        }

        //$request = $fb->request('GET', '/me/feed?fields=id,name,created_time,picture&limit=100');

//         function request(){
//
//
//
//         }

    }

?>
