<?php

  class Book {

    var $id,$title,$image;
    var $authors=array();
    var $isbns=array();
    var $reviews=array();
    var $mentions=array();

    function __construct($args=array()){

      if(is_array($args) && count($args)>0){

        if(isset($args['id']) && $this->get_book_from_id($args['id']) ){
          $this->update($args);
        } elseif(isset($args['isbns']) && (count($args['isbns']) > 0 ) ){
          if($this->get_book_from_isbn($args['isbns'][0])){
            // $this->update($args);
          } else {
            $this->create($args);
          }
        }
        foreach($args as $key => $value){
          if($key != 'id' && $key != 'isbn' && $value != ''){
            $this->{$key} = $value;
          }
        }
      } else {
        if ($this->get_book_from_id($args)){
          $this->update($args);
        } elseif($this->get_book_from_isbn($args)){
          $this->update($args);
        } elseif($this->get_book_from_isbn($args['isbns'][0])){
          $this->update($args);
        } else {
          if($this->create($args)){
            echo "created successfully.\n";
          } else {
            echo "failed to create . \n";
            var_dump($args);
          }
        }
      }
    }

    function create($args){

      $book_id = false;
      $columns = array();
      $values = array();
      if ( isset($args['title']) && isset($args['isbns']) && ( count($args['isbns'] > 0 ) ) ){
        $data = array(
          'title'=>$args['title'],
          'isbns'=>$args['isbns'],
          'authors'=>array()
        );
        global $db;
        $book_id = $db->insert_row('buzztrace_books',$data);
        $this->get_book_from_id($book_id);
      }

      return $book_id;
    }

    function update($args){
      global $db;
      $db->update_row('buzztrace_books',$this->id,$args);
      $this->get_reviews();
    }

    function get_book_from_id($book_id){
      global $db;
      $sql = "SELECT * FROM buzztrace_books WHERE id = '". $book_id ."' LIMIT 1 ";
      $results = $db->get_results($sql);

      if ($results !== false && count($results) > 0){
        $this->id = $results[0]->id;
        $this->title = $results[0]->title;
        $this->image = $results[0]->image;
        $this->authors = unserialize($results[0]->authors);
        $this->isbns = unserialize($results[0]->isbns);
      }
    }

    function get_book_from_isbn($isbn){
      global $db;
      $sql = "SELECT * FROM buzztrace_books WHERE isbns LIKE '%". $isbn ."%' LIMIT 1 ";
      $results = $db->get_results($sql);
      if ($results !== false && count($results) > 0){
        $this->id = $results[0]->id;
        $this->title = $results[0]->title;
        $this->image = $results[0]->image;
        $this->isbns = unserialize($results[0]->isbns);
        $this->authors = unserialize($results[0]->authors);

      }
      return $this->id;
    }

    function add_isbn_to_list($isbn){

      $isbn = (string) $isbn;

      $success = false;
      $found = false;
      $isbns = $this->isbns;

      foreach($isbns as $key => $value){
        if ($value == $isbn){
          $found = true;
        }
      }
      if (!$found){

        $this->isbns[] = $isbn;

        global $db;
        $serialize = serialize(array($isbn));

          if($db->update_row('buzztrace_books',$this->id,array('isbns'=>serialize($this->isbns)))){
            $success = "UPDATED";
          }

      } else {
        $success = "FOUND";
      }


      return $success;

    }

    function last_amazon_review($market=".com"){

      $last_amazon_review = strtotime('1969-12-31 00:00:00');
      foreach($this->reviews as $log_date => $review){
        if($review->domain == "www.amazon" . $market){
          $last_amazon_review = strtotime($review->log_time);
        }

      }
      return $last_amazon_review;
    }

    function get_reviews(){
      $sql = "SELECT * FROM tracker_book_reviews WHERE book_id = '". $this->id ."'";
      global $db;
      $reviews = $db->get_results($sql);
      foreach ($reviews as $review){
        $this->reviews[] = $review;

      }
    }


    function get_mentions($catagory="all",$args){
      if($catagory == "all"){
        $catagory_query = "";
      } else {
        $catagory_query = "AND sh.catagory = '". $catagory ."'";
      }

      $date_query = " AND bm.source_record_time BETWEEN '". date('Y-m-d',(time() - ( 60*60*24*30 ))) ." 00:00:00' AND '". date('Y-m-d') ." 23:59:59' ";
      if(isset($args['start']) && isset($args['end'])){
        $date_query = " AND bm.source_record_time BETWEEN '". date('Y-m-d',$args['start']) ." 00:00:00' AND '". date('Y-m-d',$args['end']) ." 23:59:59' ";
      }



      global $db;


      // $sql = "
      //   SELECT
      //     bm.book_id AS book_id,
      //     bm.domain AS domain,
      //     bm.url AS url,
      //     bm.source_record_time AS date,
      //     sh.catagory AS catagory,
      //     bm.type AS catagory_type,
      //     bm.excerpt_text AS text
      //   FROM tracker_book_mentions bm
      //     JOIN index_source_hosts sh ON bm.domain = sh.domain
      //     WHERE bm.book_id = '491'
      //     AND sh.catagory = 'social'
      //     AND bm.is_archived = 0
      //
      //     UNION
      //
      //     SELECT
      //       bm.book_id AS book_id,
      //       bm.domain AS domain,
      //       bm.url AS url,
      //       bm.source_record_time AS date,
      //       bm.type AS catagory,
      //       bm.type AS catagory_type,
      //       bm.excerpt_text
      //     FROM tracker_book_mentions bm
      //       WHERE bm.type = 'social'
      //       AND bm.type <> 'web'
      //       AND bm.is_archived = 0
      //
      //       GROUP BY date
      //
      // ";


      $sql = "
        SELECT
          bm.book_id AS book_id,
          bm.domain AS domain,
          bm.url AS url,
          bm.source_record_time AS date,
          sh.catagory AS catagory,
          bm.type AS catagory_type,
          bm.excerpt_text
        FROM tracker_book_mentions bm
          JOIN index_source_hosts sh ON bm.domain = sh.domain
          WHERE bm.book_id = '". $this->id ."'
          ". $catagory_query ."
          AND bm.is_archived = '0'
          ". $date_query ."

          UNION

          SELECT
            bm.book_id AS book_id,
            bm.domain AS domain,
            bm.url AS url,
            bm.source_record_time AS date,
            bm.type AS catagory,
            bm.type AS catagory_type,
            bm.excerpt_text
          FROM tracker_book_mentions bm
            WHERE bm.book_id = '". $this->id ."'
            AND bm.type = '". $catagory ."'
            AND bm.is_archived = '0'
            ". $date_query ."
      ";


      $sql = "
        SELECT
          bm.book_id AS book_id,
          bm.domain AS domain,
          bm.url AS url,
          bm.source_record_time AS date,
          sh.catagory AS catagory,
          bm.type AS catagory_type,
          bm.excerpt_text
        FROM tracker_book_mentions bm
          JOIN index_source_hosts sh ON bm.domain = sh.domain
          WHERE bm.book_id = '". $this->id ."'
          ". $catagory_query ."
          AND bm.is_archived = '0'
          ". $date_query ."

      ";




      // die($sql);

      $mentions = $db->get_results($sql);
      // $this->mentions[]
      foreach($mentions as $mention){
        // if($mention->catagory_type != "web"){
        //   $mention->catagory = $mention->catagory_type;
        // }

        if(strlen($mention->domain) > 55){
          $mention->domain = substr($mention->domain,0,53) . "...";
        }

        $this->mentions[] = $mention;
      }

      return $sql;

    }

  }

/*

SELECT
  bm.id AS id,
  bm.book_id AS book_id,
  bm.domain AS domain,
  bm.url AS url,
  bm.source_record_time AS date,
  sh.catagory AS catagory
FROM tracker_book_mentions bm
  JOIN index_source_hosts sh ON bm.domain = sh.domain
  WHERE bm.book_id = '30'
  AND catagory = 'news'
  ORDER BY source_record_time

*/

 ?>
