<?php

  class Sales {

    var $isbn = "0486411095"; //dracula
    var $market = ".com";//
    var $start;
     var $frequency = DAY;
    var $duration = WEEK;
    var $ranks = array();
    var $rank_set;

    function __construct($isbn,$args){
      $this->isbn = $isbn;
      if($args->start){
        $this->start = $args->start;
      } else {
        $this->start = time() - (60 * 60 * 24 * 7);
      }
      if($args->frequency){
        $this->frequency = $args->frequency;
      }
      if($args->duration){
        $this->duration = $args->duration;
      }
      if($args->market){
        $this->market = $args->market;
      }
    }

  }

   class Amazon_Sales extends Sales {
    function get_ranks(){
      global $db;
      $sql = "
        SELECT isbn, unix_timestamp(rank_time) AS rank_time , salesrank, market
        FROM tracker_amazon_salesrank
        WHERE isbn = '". $this->isbn ."'
          AND rank_time >= '". date('Y-m-d 00:00:00', $this->start) ."'
          AND salesrank > 0
          AND market = '". $this->market ."'
      ";
      $this->ranks = $db->get_results($sql,OBJ);
      return $this->ranks;
    }
    function get_current_ranks(){

    }
    function get_rank_set($args){
      $this->get_ranks();
      $ranks = array();
      $rank_time = array();
      $format = "Ymd";
      if($args->frequency == 3600){
        $format = "YmdH";
      }
      foreach($this->ranks as $record){
        $ranks[date($format, $record->rank_time)][] = $record->salesrank;
      }
      //return $ranks;
       $dates = array();
       $j = 0;
      for ($i = $args->start + $args->frequency; $i <= ($args->start + $args->duration); $i += $args->frequency){
        // $rank_avg = 0;


        if(count($ranks[date($format,$i)] > 0)){
          $rank_total = 0;
          foreach($ranks[date($format,$i)] as $record){
            $rank_total += $record;
          }
          $rank_avg = $rank_total / count($ranks[date($format,$i)]);
          $dates[date($format,$i)] = $rank_avg;
        } else {
          $dates[date($format,$i)] = "";
        }

        $dates[date($format,$i)] = $ranks[date($format,$i)][0];
        //$test++;
      }
      return $dates;
      //  $this->rank_set = $dates;
    }
  }



 ?>
